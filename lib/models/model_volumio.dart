

class Volumio{
   String idSuc;
   String status;
   String reportDate;
   String reportHour;
   int vol;

  Volumio({this.idSuc, this.status, this.reportDate, this.reportHour, this.vol});

  Volumio.fromJson(Map<String, dynamic> json){
    idSuc = json['idEquipo'];
    status = json['estado'];
    reportDate = json['fechaEstado'];
    reportHour = json['horaEstado'];
    vol = json['volumen'];
  }



}