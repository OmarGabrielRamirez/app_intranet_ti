
import 'dart:convert';
import 'package:http/http.dart' as http;


class _VolumiosProvider{

  List <dynamic> itemsVolumios = [];
  
  Future<List<dynamic>> loadingData() async{
    var jsonData = null;
    var response  = await http.get("https://intranet.prigo.com.mx/api/musica");
    jsonData = json.decode(response.body);
    Map dataMap = jsonData;
    itemsVolumios = dataMap['volumios'];
    return itemsVolumios;
  } 
  
}

final VolumiosProvider = new _VolumiosProvider();