import 'package:flutter/material.dart';

  final _icons = <String, IconData> {
    'music_note' : Icons.music_note,
    'assignment_turned_in' : Icons.assignment_turned_in,
  };

  Icon getShadow(String nameIcon){
    return Icon(_icons[nameIcon], color: Colors.black38, size: 40.0,);
 }