import 'package:flutter/material.dart';

import 'package:Prigo/main.dart';
import 'package:Prigo/pages/tickets.dart';
import 'package:Prigo/pages/volumios.dart';
import 'package:Prigo/pages/login.dart';

Map <String, WidgetBuilder> getApplicationRoutes(){
  return <String, WidgetBuilder>{
        '/': (BuildContext context) =>  MainPage(),
        'volumios': (BuildContext context) =>  VolumioPg_(),
        'tickets': (BuildContext context) =>  TicketsPage(),
        'login' : (BuildContext contex) => LoginScreen(),
  };
}
