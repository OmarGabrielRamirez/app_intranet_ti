import 'package:Prigo/utils/shadow_string_util.dart';
import 'package:flutter/material.dart';

import 'package:Prigo/routes/routes.dart';
import 'package:Prigo/pages/login.dart';
import 'package:Prigo/providers/menu_provider.dart';
import 'package:Prigo/utils/icon_string_util.dart';

import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Prigo Intranet",
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes:  getApplicationRoutes(),
      theme: ThemeData(
        primaryColor: Colors.orange,
      ),
    );
  }
}

  class MainPage extends StatefulWidget {
    @override
    _MainPageState createState() => _MainPageState();
  }

  class _MainPageState extends State<MainPage> {

  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if(sharedPreferences.getString("access_token") == null) {
    Navigator.pushNamed(context, 'login');
    }
  }

  Future logOutUser(BuildContext context) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
      preferences.getKeys();
        for(String key in preferences.getKeys()) {
          if(key == "access_token") {
            preferences.remove(key);
            Navigator.pushReplacementNamed(context, "login");
          }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: new BoxConstraints.tightFor(),
        color: Colors.white,
        child: Stack(
          children:<Widget>[
          _getBackground(),
          _getGradient(),
          _items(),
                    ],
                  ),
                ),
                floatingActionButton: FloatingActionButton(
                    backgroundColor: Colors.orange,
                    child:  Icon(
                      Icons.power_settings_new,
                      color: Colors.white),
                      onPressed: (){
                        logOutUser(context);
                      },
                  ),
              );
              // final screenSize = MediaQuery.of(context).size;
              // final screenWidth = screenSize.width;
              // final screenHeight = screenSize.height;
              // return Container(
              //       width: screenWidth,
              //       height:screenHeight ,
              //   child: Scaffold(
              //       appBar: AppBar(
              //     title: Text("Prigo Intranet - TI", style: TextStyle(color: Colors.white, fontFamily: 'Raleway')),
              //     leading: Icon(Icons.home, size: 18.0, color: Colors.white,),
              //   ),
              //       floatingActionButton: FloatingActionButton(
              //       backgroundColor: Colors.orange,
              //       child:  Icon(
              //         Icons.power_settings_new,
              //         color: Colors.white),
              //         onPressed: (){
              //           logOutUser(context);
              //         },
              //     ),
              //   body: Container(
              //     padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              //     margin: EdgeInsets.only(top:20.0),
              //     child:_items()
              //   )
              //   )
          
              // );
            }
          
            Widget _items(){
              return FutureBuilder(
                future: MenuProvider.loadingData(),
                initialData: [],
                builder: (BuildContext context, AsyncSnapshot <List<dynamic>> snapshot){
                  return ListView(
                    padding: EdgeInsets.fromLTRB(24.0, 140, 24.0, 32.0),
                    children: _itemsList(snapshot.data, context),
                  );
          
                },
              );
            }
          
          
            List<Widget>_itemsList( List<dynamic> data, BuildContext context){
               final List <Widget> itmMenu = [];
               data.forEach((opt){
                 final widgetTmp = GestureDetector(
                   onTap:  (){
                     Navigator.pushNamed(context, opt['route']);
                   },
                    child:Card(
                        shape:  RoundedRectangleBorder( borderRadius: BorderRadius.circular(10.0)),
                        margin: EdgeInsets.only(top:10.0, bottom: 10.0, left: 13, right:13),
                        elevation: 5.0,
                          child: Container(
                            margin: EdgeInsets.only(top: 30.0, bottom: 30.0),
                              child: Column(
                                children: <Widget>[
                                  Text(opt['title'],style: TextStyle(fontFamily: 'Lato', color: Colors.orange,fontSize: 17.0, fontWeight: FontWeight.bold)),
                                    SizedBox(width: 20.0, height: 10.0,),
                                  Stack(
                                    children: <Widget>[
                                      Positioned(
                                        left: 1.0,
                                        top: 2.0,
                                          child: getShadow(opt['icon']),
                                      ),
                                      getIcon(opt['icon']),
                                  ],
                                ),
                              ]
                            ),
                          ),
                    ),
                 );
                 itmMenu..add(widgetTmp);
               });
               return itmMenu;
              }
          
           Container _getBackground() {
             return new Container(
               child: Image.asset("assets/images/kayser.jpg",
               fit: BoxFit.cover,
               height: 300.0,),
             );
           }

           Container _getGradient() {
             return new Container(
              margin: new EdgeInsets.only(top: 190.0),
              height: 110.0,
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                  colors: <Color>[
                    new Color(0x00FFFFFF),
                    new Color(0xFFFFFFFF)
                  ],
                   stops: [0.0, 0.9],
                   begin: const FractionalOffset(0.0, 0.0),
                   end: const FractionalOffset(0.0, 1.0),
                ),
              )
             );
           }

           Widget _getContetn(){
             return ListView(
               padding: EdgeInsets.fromLTRB(0.0, 72.0, 0.0, 32.0),
                children: <Widget>[
                  _items(),
                ],

             );
           }


}