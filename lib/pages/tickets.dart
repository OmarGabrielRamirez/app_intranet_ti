import 'package:flutter/material.dart';

class TicketsPage extends StatelessWidget{
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Control Tickets", style: TextStyle(color: Colors.white, fontFamily: 'Lato', fontWeight: FontWeight.bold)),
        leading: Icon(Icons.assignment_turned_in, color: Colors.white, size: 18.0,),
        ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.orange,
          child:  Icon(
            Icons.subdirectory_arrow_left,
            color: Colors.white),
            onPressed: (){
              Navigator.pop(context);
            },
        ),
        body: Center(child: Text("PRÓXIMAMENTE", style: TextStyle(color: Colors.orange, fontSize: 14.0),) ),
    
    );
  }
}