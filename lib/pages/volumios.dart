import 'dart:convert';

import 'package:Prigo/models/model_volumio.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class VolumioPg_ extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _PageVolumios();
  }
  
  class _PageVolumios extends State<VolumioPg_>  {
  List<IconData> _opt = [Icons.play_arrow, Icons.pause, Icons.stop,Icons.cached,Icons.volume_up, Icons.volume_down,Icons.cancel, Icons.build];
  List<Volumio> _volumios = List<Volumio>();
  List<Volumio> _voldispl = List<Volumio>();

  String _optSelect = "IconData(U+0E869)";
  String action;
  String idSuc;
  
  var iconPause = Icon(Icons.pause_circle_outline, color:Colors.orange, size:36.0);
  var iconStop = Icon(Icons.volume_off, color: Colors.red,  size:36.0);
  var iconPlay = Icon(Icons.volume_up, color: Colors.green,  size:36.0);

  Future<List<Volumio>> getVolumios() async {
    var url = 'https://intranet.prigo.com.mx/api/musica';
    var response = await http.get(url);
    var volu = List<Volumio>();
    
    if( response.statusCode == 200){
      var volumios = json.decode(response.body);
      for(var volumio in volumios){
        volu.add(Volumio.fromJson(volumio));
      }
    }
    return volu;
  }

    void initState() {
    getVolumios().then((value) {
      setState(() {
        _volumios.addAll(value);
        _voldispl = _volumios;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Control volumios", style: TextStyle(color: Colors.white, fontFamily: 'Lato', fontWeight: FontWeight.bold)),
        leading: Icon(Icons.music_note, color: Colors.white, size: 20.0,),
      
      ),
      body: ListView.builder(
        padding: EdgeInsets.symmetric(vertical:1, horizontal:7),
        itemBuilder: (context,index){
          return index == 0 ? _searchBar() : _listVolumios(index-1);
          
        },
        itemCount: _voldispl.length+1,
        ),
  
        floatingActionButton: SpeedDial(
          marginRight: 15,
          marginBottom: 20,
          animatedIcon: AnimatedIcons.menu_close,
          animatedIconTheme: IconThemeData(size: 22.0),
          visible: true,
          closeManually: false,
          curve: Curves.easeInToLinear,
          overlayColor: Colors.black,
          overlayOpacity: 0.4,
          backgroundColor: Colors.orange,
          foregroundColor: Colors.white,
          elevation: 8.0,
          shape: CircleBorder(),
          children: [
            SpeedDialChild(
              child: Icon(Icons.home),
              backgroundColor: Colors.deepOrange,
              label: 'Inicio',
             labelBackgroundColor: Colors.orange,
              labelStyle: TextStyle(fontSize: 15.0, color:Colors.white, fontWeight: FontWeight.bold, fontFamily: 'Lato'),
              onTap: (){
                Navigator.pushReplacementNamed(context,  '/');
              }
            ),
            SpeedDialChild(
              child: Icon(Icons.cached),
              backgroundColor: Colors.deepOrange,
              label: 'Actualizar',
              labelBackgroundColor: Colors.orange,
              labelStyle: TextStyle(fontSize: 15.0, color:Colors.white, fontWeight: FontWeight.bold, fontFamily: 'Lato'),
              onTap: (){
                Navigator.pushReplacementNamed(context,  'volumios');
              },
            ),
          ],
        ),
        
    );
  }

  void iniState(){
    super.initState();
  }

  _searchBar(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal:10, vertical:20),
      child: TextField(
        decoration: InputDecoration(
          icon: Icon(Icons.search),
          labelText: 'Buscar volumio',
          labelStyle: TextStyle( fontFamily: 'Lato', fontSize: 13.0, fontWeight: FontWeight.bold)
        ),
        onChanged: (text){
          text = text.toLowerCase();

          setState(() {
             _voldispl = _volumios.where((volumio){
            var volumioId = volumio.idSuc.toLowerCase();
            return volumioId.contains(text); 
          }).toList();
            
          });

        },
      ),
      );
  }


    Icon setIcon(estado){
    if(estado =='stop'){
      return iconStop;
    }else if(estado == 'play'){
      return  iconPlay;
    }else if(estado =='pause'){
      return iconPause;
    }
  }
  

  _listVolumios(index){
          return Card(
            
            shape:  RoundedRectangleBorder( borderRadius: BorderRadius.circular(15.0)),
            elevation: 5.0,
            child: Padding(
              padding:const EdgeInsets.symmetric(horizontal: 15, vertical:5),
              
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                    ListTile(
                      title: Text(_voldispl[index].idSuc, style: TextStyle(
                      fontFamily: 'Lato',
                      fontSize: 17,
                      fontWeight: FontWeight.bold,
                      color: Colors.orange,
                      shadows: [
                          Shadow(
                            blurRadius: 2.0,
                            color: Colors.white,
                            offset: Offset(1.0, 2.0),
                          ),
                        ],
                      ),),
                      subtitle: Text( "Último reporte: " +_voldispl[index].reportDate +" " + _voldispl[index].reportHour , style: TextStyle(fontSize: 12.0,fontWeight: FontWeight.w400, color: Colors.black87, fontFamily: 'Lato')),
                      leading: setIcon(_voldispl[index].status),
                      trailing: Column(
                        children: <Widget>[
                           Text("Volumen actual: "+_voldispl[index].vol.toString(), style:TextStyle(fontSize: 11.0,fontWeight: FontWeight.w400, color: Colors.black, fontFamily: 'Lato', ) ),
                           Icon(Icons.equalizer),
                        ]
                      )
                  ),
                  _DropdownTools(_voldispl[index].idSuc, context),

              ],)
            ),
          );
          
  }
    List<DropdownMenuItem<String>> getOptDropdown(){
      List<DropdownMenuItem<String>> listOpt = new List();
      _opt.forEach((option){
            listOpt.add(DropdownMenuItem(
              child: Icon(option),
              value: option.toString()
            ));
      });
      return listOpt;
  }

  Widget _DropdownTools(String idSuc, BuildContext context){
    return Row(
      children: <Widget>[
          SizedBox(width: 20.0),
            DropdownButton(
              focusColor: Colors.orange,
              value: _optSelect,
              items: getOptDropdown(),
              onChanged: (opt){
  
                switch (opt) {
                  case 'IconData(U+0E037)':
                    action = "play";
                    idSuc = idSuc;
                    saveStatus(action, idSuc, context);
                    break;
                  case 'IconData(U+0E034)':
                    action = "pause";
                    idSuc = idSuc;
                    saveStatus(action, idSuc, context);
                    break;
                  case 'IconData(U+0E047)':
                    action = "stop";
                    idSuc = idSuc;
                    saveStatus(action, idSuc, context);
                    break;
                  case 'IconData(U+0E86A)':
                    action = "restart";
                    idSuc = idSuc;
                    saveStatus(action, idSuc, context);
                    break;    
                  case 'IconData(U+0E050)':
                    action = "volume_up";
                    idSuc = idSuc;
                    saveStatus(action, idSuc, context);
                    break;   
                  case 'IconData(U+0E04D)':
                    action = "volume_down";
                    idSuc = idSuc;
                    saveStatus(action, idSuc, context);
                    break;
                 case 'IconData(U+0E5C9)':
                    action = "decline";
                    idSuc = idSuc;
                    saveStatus(action, idSuc, context);
                    break;               
                  default:
                    break;
                }
                },
            ),
          ],
        );
  }

    saveStatus(String action, String idS, BuildContext context) async {
    Map data  = {
      'idSuc': idS,
      'Action': action
    };

    var response  = await http.post("https://intranet.prigo.com.mx/api/musica/changestatus", body: data);
    if(response.statusCode == 200){
      String jsonData = response.body.toString();
      if(jsonData == 'success'){
       _showAlert(context, idS, action, "Control volumios");
      }else if(jsonData == 'error'){

      }
    }
  }

    void _showAlert(BuildContext context, String idS, String action, String title){
        showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context){
            return AlertDialog(
              shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(10.0)),
              title: Icon(Icons.check, size: 70.0, color: Colors.green),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                   Text("Se guardó la acción " + action + ", para el equipo "+ idS +". En minutos se verá reflejado el cambio en el equipo, dependiendo de su configuración.", textAlign: TextAlign.justify,style: TextStyle( fontFamily: 'Lato', fontWeight: FontWeight.bold, fontSize: 15.0)),
                ]
              ),
              actions: <Widget>[
                FlatButton(onPressed: (){
                  Navigator.pop(context);
                }, child: Text("OK"))
              ],
            );
          }
        );
    }
}