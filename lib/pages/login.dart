import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();

}

class _LoginScreenState extends State<LoginScreen> {
  var tamIcon = 20.0;
  bool _isLoading = false; 
  @override
  Widget build(BuildContext context) {
      SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(statusBarColor: Colors.transparent));
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Colors.white, Colors.white],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter),
        ),
        child: _isLoading ? Center(
          child: SpinKitFadingCircle(
            color: Colors.orange,
            )) : ListView(
          children: <Widget>[
            _headerSection(),
            _textSection(),
            _buttonSection(context),
          ],
        ),
      ),
    );
  }
    void _showAlert(BuildContext context){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Icon(Icons.error_outline, color:Colors.red, size: 70.0),
            content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                   Text("Ingresa los datos correctamente para poder iniciar sesión.", style: TextStyle( fontWeight: FontWeight.bold, fontSize: 15.0, fontFamily: 'Lato' )),     
                ]
              ),
              actions: <Widget>[
                FlatButton(onPressed: (){
                  Navigator.pop(context);
                }, child: Text("OK", style: TextStyle(color:Colors.orange),))
              ],
          );
        }
      );

    }

  Container _headerSection(){
    return Container(
      margin: EdgeInsets.only(top: 40.0, bottom: 60.0),
      padding: EdgeInsets.all(65.0),
      child: Image.asset("assets/images/prigo_logo.png",
        fit: BoxFit.contain
      ),
    );
  }

  Container _textSection(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30.0),
      margin: EdgeInsets.only(top: 10.0),
      child: Column(
        children: <Widget>[
          _txtEmail(),
          SizedBox(height: 30.0,),
          _txtPassword(),
        ],
      ),
    );
  }

  Widget _txtEmail() {
    return TextField(
      cursorColor: Colors.orange,
      autofocus: true,
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(left:5.0, right:5.0),
        hintText: 'ejemplo@prigo.com.mx',
        hintStyle: TextStyle(fontFamily: 'Lato', fontWeight: FontWeight.bold),
        labelText: 'Email',
        labelStyle: TextStyle(fontFamily: 'Lato', fontWeight: FontWeight.bold),
        icon: Icon( Icons.email, size: 18.0, )
      ),
      onChanged: (valor) =>setState(() {
      })
    );
  }

    Widget _txtPassword(){
     return TextField(
      cursorColor: Colors.orange,
      obscureText: true,
      controller: _passwordController,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(left:5.0, right:5.0),
        hintText: 'Contraseña',
        hintStyle: TextStyle(fontFamily: 'Lato', fontWeight: FontWeight.bold),
        labelText: 'Contraseña',
        labelStyle: TextStyle(fontFamily: 'Lato', fontWeight: FontWeight.bold),
        icon: Icon( Icons.vpn_key, size: 18.0,)
      ),
      onChanged: (valor) =>setState(() {
      })
    );
  }
  Container _buttonSection(BuildContext context){
    return Container (
      height: 46.0,
      padding: EdgeInsets.only(left:20.0, right:20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: RaisedButton(
      elevation: 2.0,  
      onPressed: (){
            setState(() {
            _isLoading = true;
          });
          signIn(_emailController.text, _passwordController.text, context);
      },
        color: Colors.white,
      
        shape: RoundedRectangleBorder(
           borderRadius: new BorderRadius.circular(10.0),
           side: BorderSide(color: Colors.orange, width: 2.0)
),
        child: Text("Ingresar", style : TextStyle(color: Colors.orange ,fontFamily: 'Lato', fontWeight: FontWeight.bold)),
      ),
    );
  }

  signIn(String email, String password, BuildContext context) async {
    Map data = {
      'email': email,
      'password': password
    };

    var jsonData = null;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    
    var response = await http.post("https://intranet.prigo.com.mx/api/auth/login", body: data);


    if(response.statusCode == 200){
        jsonData = json.decode(response.body);
        if(jsonData != null){
          setState(() { _isLoading = false; });
        }
        sharedPreferences.setString("access_token", jsonData['access_token']);
       Navigator.pushReplacementNamed(context, '/');
    }else{

  
      setState(() {
      _isLoading = false;
      });
     _showAlert(context);
  
    }
    
  }

  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
}